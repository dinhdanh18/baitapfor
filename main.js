//TÌM SỐ NGUYÊN DƯƠNG NHỎ NHẤT
document.getElementById('btnBai1').addEventListener('click',function(){
    var ketQua = document.getElementById('ketQuaBai1');
    var sum = 0;
    var i = 0;
    while(sum<10000){
        i++;
        sum += i;
    }
    // for(i = 0; sum < 10000; i++){
    //     sum = sum + i;
    // }
    ketQua.innerHTML = `Số nguyên dương nhỏ nhất là ${i} và tổng là ${sum}` ;
});
//Viết chương trình nhập vào 2 số x, n tính tổng: S(n) = x + x^2+ x^3 + … + x^n (Sử dụng vòng lặp và hàm)
document.getElementById('btnBai2').addEventListener('click',function(){
    var ketQua = document.getElementById('ketQuaBai2');
    var x = document.getElementById('xBai2').value*1;
    var n = document.getElementById('nBai2').value*1;
    var tong = 0;
    var luyThua = 1;
    for(i = 1;i<=n ; i++){
        luyThua = luyThua * x;
        tong = tong + luyThua;
    }
    ketQua.innerHTML = `Tổng là: ${tong}`

});
// Tính giai thừa
document.getElementById('btnBai3').addEventListener('click',function(){
    var ketQua = document.getElementById('ketQuaBai3');
    var n = document.getElementById('nBai3').value*1;
    var giaiThua = 1;
    for(var i = 1 ; i <= n ; i++){
        giaiThua = giaiThua *i;
    }
    ketQua.innerHTML = `Giai Thừa: ${giaiThua}`
});
//Tạo 10 thẻ div xanh đỏ theo chẵn lẻ
document.getElementById('btnBai4').addEventListener('click',function(){
    var content = document.getElementById('content');
    var s = "";
    for(i = 1; i<=10;i++ ){
        if(i % 2 == 0){
        s =s + `<div class="do p-2 my-2">div đỏ ${i}</div>`
        }
        if(i % 2 == 1){
        s =s+ `<div class="xanh p-2 my-2">div xanh ${i}</div>`
    }
    content.innerHTML = s;
    }
})

//Bài tập thêm
// Đếm số chẵn lẽ
document.getElementById('btnChanLe').addEventListener('click',function(){
    var ketQua = document.getElementById('ketQuaChanLe');
    soChan = "";
    soLe = "";
    
    for(i = 0; i<100;i++){
        if(i % 2 == 0){
            soChan = soChan + " " + i;
        }else{
            soLe = soLe + " " + i;
        }
    }
    ketQua.innerHTML = `${soChan} <br/> ${soLe} `
});
//Đếm số chữ số chia hết cho 3 nhỏ hơn 1000
document.getElementById('btnChia3').addEventListener('click',function(){
    var ketQua = document.getElementById('ketQuaChia3');
    var count = 0;
    for(i=0;i < 1000 ; i++){
        if(i%3==0){
            count ++;
        }
    }
    ketQua.innerHTML = `Có ${count} chữ số chia hết cho 3 nhỏ hơn 1000`
});